package codingBat;

import java.util.List;
import java.lang.CharSequence.*;

public class Functional_1 {


    /* Functional-1 > square
    Given a list of integers, return a list where each integer is multiplied with itself.

    square([1, 2, 3]) → [1, 4, 9]
    square([6, 8, -6, -8, 1]) → [36, 64, 36, 64, 1]
    square([]) → []
    */
    public List<Integer> square(List<Integer> nums) {
        nums.replaceAll(s -> s * s);
        return nums;
    }


    /*Functional-1 > addStar
    Given a list of strings, return a list where each string has "*" added at its end.

    addStar(["a", "bb", "ccc"]) → ["a*", "bb*", "ccc*"]
    addStar(["hello", "there"]) → ["hello*", "there*"]
    addStar(["*"]) → ["**"]
    */
    public List<String> addStar(List<String> strings) {
        strings.replaceAll(s -> s + "*");
        return strings;
    }


    /* Functional-1 > noX
    Given a list of strings, return a list where each string has all its "x" removed.

    noX(["ax", "bb", "cx"]) → ["a", "bb", "c"]
    noX(["xxax", "xbxbx", "xxcx"]) → ["a", "bb", "c"]
    noX(["x"]) → [""]
    */

    public List<String> noX(List<String> strings) {
        strings.replaceAll(s -> s.replaceAll("x", ""));
        return strings;
    }
}
