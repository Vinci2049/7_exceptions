package com.epam.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrey_Vaganov on 12/5/2016.
 */
public class MainReader {

    /**
     * Формат даты
     */
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

    /**
     * Форматтер, используется для преобразования строк в даты и обратно
     */
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);


    /**
     * Точка входа в программу
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            readFile();
        } catch (SpecialIOException e) {
            System.out.println(e.getMessage() + " " + e.getFileName());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
            e.printStackTrace();
        }
    }


    static class SpecialIOException extends Exception {

        private String fileName;

        private String getFileName() {
            return fileName;
        }

        public SpecialIOException(String message, String fName) {
            super(message);
            fileName = fName;
        }
    }

    /**
     * Метод для чтения дат из файла
     */
    public static void readFile() throws SpecialIOException, IOException {

        String fileName = "file.txt";
        //Открываем потоки на чтение из файла
        try (FileReader reader = new FileReader(fileName)) {
            BufferedReader byfReader = new BufferedReader(reader);

            //Читаем первую строку из файла
            String strDate = byfReader.readLine();

            while (strDate != null) {

                Date date = parseDate(strDate);

                //Выводим дату в консоль в формате dd-mm-yy
                System.out.printf("%1$td-%1$tm-%1$ty \n", date);

                //Читаем следующую строку из файла
                strDate = byfReader.readLine();
            }

        } catch (FileNotFoundException e) {
            throw new SpecialIOException("Файл не найден:", fileName);
        }
    }

    /**
     * Метод преобразует строковое представление даты в класс Date
     *
     * @param strDate строковое представление даты
     * @return
     */
    public static Date parseDate(String strDate) {

        try {
            return dateFormatter.parse(strDate);
        } catch (Exception e) {
            System.out.println("Некорректная дата, преобразование не возможно: " + strDate);
        }
        return null;
    }
}